## Motak

Hauetako bat izan behar da:

* `feat`: A new feature
* `fix`: A bug fix
* `docs`: Documentation only changes
* `style`: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* `refactor`: A code change that neither fixes a bug nor adds a feature
* `perf`: A code change that improves performance
* `test`: Adding missing or correcting existing tests
* `chore`: Changes to the build process or auxiliary tools and libraries such as documentation generation

### Commit mota eta semver bertsioak

[Semver](https://semver.org/)-ek (semantic-versioning) 3 zenbaki erabiltzen ditu, puntuaz banatuta: `MAJOR.MINOR.PATCH`, adib `1.12.3`.

Release bat martxan jartzean (master-en dev adarra mergeatu) commit berri guztiak aztertuko dira eta honela egingo da:

* `fix`-en bat badago `PATCH` bertsioa +1 (`1.12.4`)
* `feat`-en bat badago `MINOR` bertsio +1 (`1.13.0`)
* `Breaking change` badago `MAYOR` bertsioa +1 (`2.0.0`)

`chore`, `refactor` eta besteek ez dute bertsio berria sortzen.