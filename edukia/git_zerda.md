## What is Git

- Distributed Version Control System
- Great branching model that adapts well to most workflows
- Fast and reliable
- Keeps a complete history
- Disaster recovery friendly
- Open Source
