## Git Workflow

- Igo gabeko fitxategiak
    - New files that Git has not been told to track previously.
- Aldatutako fitxategiak (Workspace)
    - Files that have been modified but are not committed.
- Staging area (Index)
    - Modified files that have been marked to go in the next commit.
- Eguneratua
    - Hosted repository on a shared server
